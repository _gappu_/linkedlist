#ifndef LINKEDLIST_H
#define LINKEDLIST_H

/**
 * @brief The Element struct элемент списка
 */
struct Element {
    int value;      ///< значение
    Element *p;     ///< указатель на произвольный элемент
    Element *next;  ///< указатель на следующий элемент

    /**
     * @brief Element конструктор по умолчанию
     * @param _val значение
     */
    explicit Element(int val = 0);
};

class LinkedList
{
public:
    /**
     * @brief LinkedList конструктор по умолчанию
     */
    LinkedList();
    ~LinkedList();      ///< деструктор

    /**
     * @brief LinkedList конструктор копирования
     * @param lst копируемый список
     */
    LinkedList(const LinkedList &lst);

    /**
     * @brief operator оператор присваивания копированием
     * @param lst присваиваемый список
     * @return список
     */
    LinkedList& operator=(LinkedList lst);

    /**
     * @brief LinkedList конструктор перемещения
     * @param lst перемещаемый список
     */
    LinkedList(LinkedList &&lst);

    /**
     * @brief operator = оператор присваивания перемещением
     * @param lst перемещаемый список
     * @return список
     */
    LinkedList& operator=(LinkedList &&lst);

    /**
     * @brief isEmpty является ли список пустым
     * @return признак, является ли список пустым
     */
    bool isEmpty();

    /**
     * @brief clear очистить список
     */
    void clear();

    /**
     * @brief append добавить значение в список
     * @param val значение
     * @param index произвольный индекс
     */
    void append(int val, int index = 0);

    /**
     * @brief size получить размер списка
     * @return размер списка
     */
    int size();

    /**
     * @brief at получить элемент списка
     * @param index индекс элемента
     * @return  элемент списка
     */
    Element *at(int index);

    /**
     * @brief print распечатать список
     */
    void print();

    /**
     * @brief random получить случайное число от low до high
     * @param low минимальное число
     * @param high максимальное число
     * @return случайное число
     */
    static int random(int low, int high);

private:
    Element *first;     ///< первый элемент списка
    Element *last;      ///< последний элемент списка
};

#endif // LINKEDLIST_H
