#include "linkedlist.h"

#include <QDebug>

Element::Element(int val) :
    value(val),
    p(nullptr),
    next(nullptr)
{
}

LinkedList::LinkedList() :
    first(nullptr),
    last(nullptr)
{
}

LinkedList::~LinkedList()
{
    clear();
}

LinkedList::LinkedList(const LinkedList &lst)
{
    first = last = lst.first;
    Element *el = lst.first->next;
    while (el) {
        this->append(el->value);
        el = el->next;
    }
}

LinkedList &LinkedList::operator=(LinkedList lst)
{
    if (this == &lst)
        return *this;

    clear();

    first = last = lst.first;
    Element *el = lst.first->next;
    while (el) {
        this->append(el->value);
        el = el->next;
    }

    return *this;
}

LinkedList::LinkedList(LinkedList &&lst)
{
    first = last = lst.first;
    Element *el = lst.first->next;
    while (el) {
        this->append(el->value);
        el = el->next;
    }

    lst.clear();
}

LinkedList &LinkedList::operator=(LinkedList &&lst)
{
    if (this == &lst)
        return *this;

    clear();

    first = last = lst.first;
    Element *el = lst.first->next;
    while (el) {
        this->append(el->value);
        el = el->next;
    }

    lst.clear();

    return *this;
}

bool LinkedList::isEmpty()
{
    return first == nullptr;
}

void LinkedList::clear()
{
    while (first) {
        last = first->next;
        delete first;
        first = last;
    }
}

void LinkedList::append(int val, int index)
{
    Element *el = new Element (val);

    if (isEmpty()) {
        first = last = el;
    }
    else {
//        el->p = last;
        if (!index)
            el->p = last;
        else
            el->p = at(index);

        last->next = el;
        last = el;

        if (size() > 2)
            first->p = el;
    }
}

int LinkedList::size()
{
    if (this->isEmpty())
        return 0;

    int length = 0;
    Element *el = first;
    while (el) {
        ++length;
        el = el->next;
    }

    return length;
}

Element *LinkedList::at(int index)
{
    int currentIndex = 0;
    Element *el = first;
    while (el) {
        ++currentIndex;

        if (currentIndex == index)
            break;

        el = el->next;
    }

    return el;
}

void LinkedList::print()
{
    if (this->isEmpty()) {
        qWarning() << "list is empty";
        return;
    }

    Element *el = first;
    while (el) {
        qDebug().noquote() << el->value << " " << "prev =" << el->p->value;
        el = el->next;
    }
    qDebug() << "size =" << size();
}

int LinkedList::random(int low, int high)
{
    return (qrand() % ((high + 1) - low) + low);
}
