#include <QCoreApplication>

#include <QDebug>
#include <linkedlist.h>

static LinkedList *generateList() {
    LinkedList *list = new LinkedList();

    for (int i = 0; i < 10; ++i)
        list->append(LinkedList::random(10, 50), LinkedList::random(0, list->size()));

    return list;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // конструктор по умолчанию
    LinkedList *list = new LinkedList();
    for (int i = 0; i < 10; ++i)
        list->append(LinkedList::random(10, 50), LinkedList::random(0, list->size()));
    qDebug() << "main";
    list->print();

    // конструктор копирования
    LinkedList copied(*list);
    qDebug() << "copied";
    copied.print();

    // оператор присваивания
    LinkedList *assigned = list;
    qDebug() << "assigned";
    assigned->print();

    // конструктор и оператор перемещения
    LinkedList *moved = generateList();
    qDebug() << "moved";
    moved->print();

    return a.exec();
}
