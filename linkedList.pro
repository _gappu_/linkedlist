QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

SOURCES += \
        linkedlist.cpp \
        main.cpp

HEADERS += \
    linkedlist.h
